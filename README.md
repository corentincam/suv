# Signalisation Universelle pour Vélo

Le projet a été écrit et paramétré avec Platformio, voici la démarche à suivre pour le compiler et le charger sur la carte :

Installer Platformio core

`pip install platformio`

Compiler le projet

`platformio run`

Charger le firmware sur l'Arduino

`platformio run --target upload`

Le projet platformio est pré-configuré pour installer les dépendances et charger le code sur une Arduino UNO.
