class ControleurInterface {
    public:
        virtual void run() = 0;
};