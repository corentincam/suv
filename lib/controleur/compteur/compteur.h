#include "api.h"
#include "ControleurInterface.h"

#define TOTAL_DISTANCE_ADDRESS 0
#define AVERAGE_SPEED_ADDRESS 10
#define SPEED_SAMPLES_ADDRESS 20

#define COMPUTE_DELAY 500
#define REFRESH_DELAY 700
#define SAVE_DELAY 60000

enum State {
    WAIT_GPS,
    COURANT,
    TOTAL
};

class ControleurCompteur : public ControleurInterface {
    private:
        enum State state;
        GPS gps;
        LCD lcd;
        boutton btn;
        double total_distance;
        double current_distance;
        double average_speed;
        double current_speed;
        unsigned int speed_samples;
        unsigned long refresh_start_time;
        unsigned long compute_start_time;
        unsigned long save_start_time;
        bool button_state;

        void recomputeAverageSpeed();
    public:
        ControleurCompteur();
        void run();
};