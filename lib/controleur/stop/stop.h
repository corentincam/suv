#include "api.h"
#include "ControleurInterface.h"

#define STOP_ACC_THRESHOLD 340

#define STOP_COMPUTE_DELAY 100
#define AFTER_STOP_DELAY 1000

class ControleurStop : public ControleurInterface {
    private:
        Accelerometre acc;
        LEDS leds;
        unsigned long compute_start_time;
        unsigned long stop_start_time;
    public:
        ControleurStop();
        void run();
};