#include "stop.h"


ControleurStop::ControleurStop() {
    this->acc = Accelerometre();
    this->leds = LEDS();
    unsigned long time = millis();
    this->compute_start_time = time;
    this->stop_start_time = time;
}

void ControleurStop::run() {
    unsigned long time = millis();
    if (time - this->compute_start_time >= STOP_COMPUTE_DELAY) {
        this->compute_start_time = time;
        if (acc.getY() < STOP_ACC_THRESHOLD) {
            this->stop_start_time = time;
            this->leds.allumerArriere();
        }
    }
    if (time - this->stop_start_time >= AFTER_STOP_DELAY) {
        this->leds.eteindreArriere();
    }
}