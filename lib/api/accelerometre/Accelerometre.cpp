#include "Accelerometre.h"

int Accelerometre::getX() {
    return analogRead(X_PIN);
}

int Accelerometre::getY() {
    return analogRead(Y_PIN);
}

int Accelerometre::getZ() {
    return analogRead(Z_PIN);
}