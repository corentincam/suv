#include <Arduino.h>

#define X_PIN A0
#define Y_PIN A1
#define Z_PIN A2

class Accelerometre {
    public:
        int getX();
        int getY();
        int getZ();
};