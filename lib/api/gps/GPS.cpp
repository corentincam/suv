#include "GPS.h"
byte Address;

float decimalNumber(int n) {
    if (n < 10) return 0.1;
    if (n < 100) return 0.01;
    if (n < 1000) return 0.001;
    if (n < 10000) return 0.0001;
    if (n < 100000) return 0.00001;
    if (n < 1000000) return 0.000001;
    if (n < 10000000) return 0.0000001;
    if (n < 100000000) return 0.00000001;
    if (n < 1000000000) return 0.000000001;
    return 0.0000000001;
}

float GPS::getVitesse() {
    float vitesse;

    Address = 54;                                       // Point to Speed 2 register
    int intPart = this->GetSingle();                                 // Read registers from GPM
    
    Address = 55;                                       // Point to Speed 3 register
    int fracPart = this->GetSingle();                                 // Read registers from GPM
    
    vitesse = intPart + (fracPart*decimalNumber(fracPart));

    return vitesse;
}

int GPS::GetDouble(){				      // Get double register value from GPM
    int Value = 0; 
    byte H_Byte = 0;
    byte L_Byte = 0;

    Wire.beginTransmission(GPM);
    Wire.write(Address);				      // Send register start address
    Wire.endTransmission();

    Wire.requestFrom(GPM, 2);			      // Request double register
    while(Wire.available() < 2);			      // Wait for double byte
    H_Byte = Wire.read();                               // Store one
    L_Byte = Wire.read();                               // Store two

    Value = (H_Byte * 10) + L_Byte;                     // Adjust for one byte

    return(Value);	                          
}

int GPS::GetSingle(){				      // Get single register value from GPM

    int Value = 0; 

    Wire.beginTransmission(GPM);
    Wire.write(Address);				      // Send register start address
    Wire.endTransmission();

    Wire.requestFrom(GPM, 1);			      // Request register
    while(Wire.available() < 1);			      // Wait for single byte
    Value = Wire.read();                                // and store

    return(Value);	                          
}

bool GPS::isTriangulated(){

    // Latitude 

    Address = 14;                                       
    int latA = this->GetSingle();                           
    
    Address = 15;                                      
    int latB = this->GetSingle();

    Address = 16;                                     
    int latC = this->GetSingle();

    Address = 16;                                     
    int latD = this->GetSingle();

    Address = 17;                                     
    int latE = this->GetSingle();

    // Longitude 

    Address = 23;                                       
    int lonA = this->GetSingle();                           
    
    Address = 24;                                      
    int lonB = this->GetSingle();

    Address = 25;                                     
    int lonC = this->GetSingle();

    Address = 26;                                     
    int lonD = this->GetSingle();

    Address = 27;                                     
    int lonE = this->GetSingle();

    return (latA + latB + latC + latD + latE != 0) && 
    (lonA + lonB + lonC + lonD + lonE != 0);
}
